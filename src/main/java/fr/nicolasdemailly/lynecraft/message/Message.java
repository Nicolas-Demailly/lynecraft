package fr.nicolasdemailly.lynecraft.message;

import java.util.ArrayList;
import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import fr.lyneteam.nico.database.DatabaseData;
import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.command.TimedCommand;
import fr.nicolasdemailly.lynecraft.setting.Setting;
import fr.nicolasdemailly.lynecraft.setting.SettingValue;

public enum Message {
	BROADCAST(new MessageMethod() {
		@Override
		public String work(LyneCraft plugin, Player sender, Player receiver, String message) {
			return message;
		}
	}, Setting.CHAT.getDefault()),
	LOG(new MessageMethod() {
		@Override
		public String work(LyneCraft plugin, Player sender, Player receiver, String message) throws MessageException {
			if (Setting.VANISH.getValues()[1].has(sender)) if (!receiver.hasPermission("moderation.vanish")) throw new MessageException(false); else message = ChatColor.GRAY + "" + ChatColor.ITALIC + "INVISIBLE " + ChatColor.RESET + message;
			receiver.getScoreboard().getScores(ChatColor.GREEN + "en ligne").iterator().next().setScore(plugin.getServer().getOnlinePlayers().size());
			return message;
		}
	}, Setting.LOG.getDefault()),
	CHAT(new MessageMethod() {
		@Override
		public String work(LyneCraft plugin, Player sender, Player receiver, String message) throws MessageException {
			if (!(sender == null)) {
				String mute = plugin.getPlayer(sender).getData("mute.time", new DatabaseData("")).getAsString();
				if (!mute.isEmpty()) {
					String time = ChatColor.GOLD + "ind�termin�e";
					if (!mute.equals("infinite")) try {
						Date date = TimedCommand.get(mute);
						if (date.after(new Date())) time = "de " + ChatColor.GOLD + TimedCommand.format(date); else throw new Exception();
					} catch (Exception exception) {
						time = null;
					}
					if (!(time == null)) throw new MessageException(true, ChatColor.RED + "Vous �tes priv� de parole pendant une dur�e " + time + ChatColor.RED + " pour " + ChatColor.GOLD + plugin.getPlayer(sender).getData("mute.reason", new DatabaseData("non respect des r�gles du serveur")).getAsString() + ".");
				}
				if (!sender.equals(receiver) && message.contains(receiver.getName())) {
					message = message.replaceAll("(?i)@" + receiver.getName(), receiver.getName()).replaceAll("(?i)" + receiver.getName(), ChatColor.BLUE + "@" + receiver.getName() + ChatColor.RESET);
					if (Setting.SOUND.getDefault().has(receiver)) receiver.playSound(receiver.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
				}
			}
			if (Setting.VANISH.getValues()[1].has(sender)) if (!receiver.hasPermission("moderation.vanish")) throw new MessageException(false); else return ChatColor.GRAY + "" + ChatColor.ITALIC + "INVISIBLE " + ChatColor.RESET + plugin.getGroup(sender).chat(sender, message);
			return plugin.getGroup(sender).chat(sender, message);
		}
	}, Setting.CHAT.getDefault()),
	PRIVATE(new MessageMethod() {
		@Override
		public String work(LyneCraft plugin, Player sender, Player receiver, String message) throws MessageException {
			if (!(sender == null)) {
				if (plugin.getPlayer(sender).getData("block", new DatabaseData(new ArrayList<String>())).getAsListString().contains(receiver.getUniqueId().toString())) throw new MessageException(true, ChatColor.RED + "Vous avez bloqu� le joueur " + receiver.getName() + ".");
				if (plugin.getPlayer(receiver).getData("block", new DatabaseData(new ArrayList<String>())).getAsListString().contains(sender.getUniqueId().toString())) throw new MessageException(true, ChatColor.RED + "Le joueur " + receiver.getName() + " vous a bloqu�.");
			}
			sender.sendMessage(ChatColor.GRAY + "[" + ChatColor.WHITE + "Vous" + ChatColor.GRAY + " -> " + ChatColor.WHITE + receiver.getName() + ChatColor.GRAY + "] " + ChatColor.WHITE + message);
			return ChatColor.GRAY + "[" + ChatColor.WHITE + sender.getCustomName() + ChatColor.GRAY + " -> " + ChatColor.WHITE + "Vous" + ChatColor.GRAY + "] " + ChatColor.WHITE + message;
		}
	}, Setting.PRIVATE.getDefault()),
	DEATH(new MessageMethod() {
		@Override
		public String work(LyneCraft plugin, Player sender, Player receiver, String message) throws MessageException {
			return ChatColor.GRAY + message;
		}
	}, Setting.CHAT.getDefault(), Setting.DEATH.getDefault());
	
	private final MessageMethod method;
	private final SettingValue[] settings;
	
	private Message(MessageMethod method, SettingValue... settings) {
		this.method = method;
		this.settings = settings;
	}
	
	public boolean send(LyneCraft plugin, Player sender, Player receiver, String message) {
		try {
			if (!(sender == null)) for (SettingValue value : this.settings) if (!value.has(sender)) throw new MessageException(false);
			for (SettingValue value : this.settings) if (!value.has(receiver)) throw new MessageException(false);
			message = this.method.work(plugin, sender, receiver, message);
			receiver.sendMessage(message);
			return true;
		} catch (MessageException exception) {
			if (!(exception.getMessage() == null) && !(sender == null)) sender.sendMessage(exception.getMessage());
			return !exception.isStoped();
		}
	} 
	
	private interface MessageMethod {
		public String work(LyneCraft plugin, Player sender, Player receiver, String message) throws MessageException;
	}
}