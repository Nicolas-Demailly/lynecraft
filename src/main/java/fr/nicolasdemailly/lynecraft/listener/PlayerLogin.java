package fr.nicolasdemailly.lynecraft.listener;

import java.util.Date;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerLoginEvent.Result;

import fr.lyneteam.nico.database.DatabaseData;
import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.command.TimedCommand;

public class PlayerLogin extends Listener {
	public PlayerLogin(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void playerLogin(PlayerLoginEvent event) {
		DatabaseLink link = this.getPlugin().getPlayer(event.getPlayer());
		if (link == null) {
			event.setResult(Result.KICK_OTHER);
			return;
		}
		String ban = link.getData("ban.time", new DatabaseData("")).getAsString();
		if (!ban.isEmpty()) {
			String time = ChatColor.GRAY + "La fin de cette exclusion " + ChatColor.YELLOW + "n'est pas d�finie";
			if (!ban.equals("infinite")) try {
				Date date = TimedCommand.get(ban);
				if (date.after(new Date())) time = ChatColor.GRAY + "Vous pourrez r�acc�der au serveur dans\n" + ChatColor.YELLOW + TimedCommand.format(date); else throw new Exception();
			} catch (Exception exception) {
				time = null;
			}
			if (!(time == null)) {
				event.setResult(Result.KICK_BANNED);
				event.setKickMessage(
						ChatColor.RED + "\n\n-------------" + ChatColor.DARK_RED + "[" + ChatColor.GOLD + " Rapport de bannisement " + ChatColor.DARK_RED + "]" + ChatColor.RED + "-------------\n\n" +
						ChatColor.GRAY + "Bonjour, vous �tes acctuellement bannis des serveurs\n" +
						ChatColor.GRAY + "Lyne-Craft pour " + ChatColor.YELLOW + link.getData("ban.reason").getAsString() + ChatColor.GRAY + ".\n\n" +
						ChatColor.GRAY + time + ChatColor.GRAY + ".\n\n" +
						ChatColor.GRAY + "Si vous n'�tes pas en accord avec cette sanction,\n" +
						ChatColor.GRAY + "merci de d�poser une plainte � cette adresse :\n" +
						ChatColor.DARK_AQUA + ChatColor.UNDERLINE + "complain.lynecraft.fr\n\n" +
						ChatColor.RED + "------------------------------------------------\n\n");
			}
		}
		if (!event.getResult().equals(Result.ALLOWED)) this.getPlugin().removePlayer(event.getPlayer().getUniqueId());
	}
}