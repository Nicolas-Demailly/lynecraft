package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public class Kick extends Command {
	public Kick(LyneCraft plugin) {
		super(plugin, "kick");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender.hasPermission("moderation.kick")) {
			if (arguments.length > 0) {
				Player player = this.getPlugin().getServer().getPlayer(arguments[0]);
				if (!(player == null)) {
					String message = " non respect des r�gles du serveur";
					if (arguments.length > 1) {
						message = "";
						for (int index = 1; index < arguments.length; index++) message += " " + arguments[index];
					}
					player.kickPlayer(
									ChatColor.RED + "--------------" + ChatColor.DARK_RED + "[" + ChatColor.GOLD + " Rapport d'exclusion " + ChatColor.DARK_RED + "]" + ChatColor.RED + "--------------\n\n" +
									ChatColor.GRAY + "Bonjour, vous venez d'�tre exclus des serveurs\n" +
									ChatColor.GRAY + "Lyne-Craft pour " + ChatColor.YELLOW + message.substring(1) + ChatColor.GRAY + ".\n\n" +
									ChatColor.GRAY + "Merci de bien vouloir corriger votre comportement\n" +
									ChatColor.GRAY + "ou des sanction plus lourdes s'appliquerons.\n\n" +
									ChatColor.RED + "-----------------------------------------------");
					sender.sendMessage(ChatColor.GREEN + "Le joueur a �t� exclu.");
				} else sender.sendMessage(ChatColor.RED + "Ce joueur n'est pas connect�.");
				return true;
			}
		} else return this.noPermission(sender);
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		List<String> temporaly = new ArrayList<String>();
		if (sender.hasPermission("moderation.kick")) {
			if (arguments.length == 1) for (Player player : this.getPlugin().getServer().getOnlinePlayers()) temporaly.add(player.getName());
			if (arguments.length == 2) {
				temporaly.add("non respect des r�gles du serveur");
				temporaly.add("messages contrevers�s");
				temporaly.add("comportement inappropri�");
			}
		}
		return temporaly;
	}
}