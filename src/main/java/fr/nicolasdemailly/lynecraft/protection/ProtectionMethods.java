package fr.nicolasdemailly.lynecraft.protection;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public interface ProtectionMethods {
	public boolean work(LyneCraft plugin, Player player, Location location);
}