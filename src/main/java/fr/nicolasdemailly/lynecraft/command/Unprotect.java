package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.lyneteam.nico.database.DatabaseData;
import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.protection.ProtectionInformation;

public class Unprotect extends Command {
	public Unprotect(LyneCraft plugin) {
		super(plugin, "unprotect");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			ProtectionInformation information = this.getPlugin().getProtection().getChunk(player.getChunk(), false);
			if (!(information.getOwner() == null)) {
				if (sender.hasPermission("moderation.protection.remove") || information.getOwner().equals(player.getUniqueId())) try {
					final UUID owner = information.getOwner();
					information.remove(this.getPlugin().getDatabase());
					DatabaseLink link = this.getPlugin().getOfflinePlayer(owner);
					link.setData("chunks", String.valueOf(link.getData("chunks", new DatabaseData(4)).getAsInteger() + 1));
					link.saveDatabaseData();
					player.sendMessage(ChatColor.GREEN + "Protection supprim�e !");
				} catch (Exception exception) {
					sender.sendMessage(ChatColor.RED + "Impossible de supprimer cette zone pour le moment, contacter l'administration.");
					exception.printStackTrace();
				} else sender.sendMessage(ChatColor.RED + "Vous n'�tes pas le propri�taire de la zone.");
			} else sender.sendMessage(ChatColor.RED + "Cette zone n'est pas prot�g�e."); 
		} else return this.noPermission(sender);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		return new ArrayList<String>();
	}
}