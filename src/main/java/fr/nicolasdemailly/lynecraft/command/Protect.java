package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldedit.LocalSession;
import com.sk89q.worldedit.Vector;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.regions.Region;

import fr.lyneteam.nico.database.DatabaseData;
import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.inventory.Inventory;
import fr.nicolasdemailly.lynecraft.protection.ProtectionInformation;
import fr.nicolasdemailly.lynecraft.protection.ProtectionType;

public class Protect extends Command {
	public Protect(LyneCraft plugin) {
		super(plugin, "protect");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (arguments.length > 0) {
				ProtectionInformation information = this.getPlugin().getProtection().getChunk(player.getChunk(), false);
				if (arguments[0].equalsIgnoreCase("list")) {
					Inventory.PROTECTION.open(player);
				} else if (arguments[0].equalsIgnoreCase("info")) {
					if (sender.hasPermission("moderation.protection.info")) {
						if (ProtectionType.PUBLIC.is(this.getPlugin(), player.getLocation(), null)) {
							sender.sendMessage(ChatColor.GRAY + "Type de protection : " + ChatColor.YELLOW + "Publique");
						} else if (!(information.getOwner() == null)) {
							sender.sendMessage(ChatColor.GRAY + "Type de protection : " + ChatColor.YELLOW + "Priv�e");
							sender.sendMessage(ChatColor.GRAY + "Propri�taire : " + ChatColor.YELLOW + this.getPlugin().getServer().getOfflinePlayer(information.getOwner()).getName());
							if (information.getPlayers().size() > 1) {
								sender.sendMessage(ChatColor.GRAY + "Joueurs ajout�s " + ChatColor.YELLOW + "(" + (information.getPlayers().size() - 1) + ")");
								for (UUID uuid : information.getPlayers()) if (!uuid.equals(information.getOwner())) sender.sendMessage(ChatColor.GRAY + "- " + ChatColor.YELLOW + this.getPlugin().getServer().getOfflinePlayer(uuid).getName());
							}
						} else sender.sendMessage(ChatColor.GRAY + "Type de protection : " + ChatColor.YELLOW + "Aucune");
						return true;
					}
					return this.noPermission(sender);
				} else if (arguments[0].equalsIgnoreCase("public")) {
					if (sender.hasPermission("moderation.protection.public")) try {
						Plugin plugin = this.getPlugin().getServer().getPluginManager().getPlugin("WorldEdit");
						if (!(plugin == null) && plugin instanceof WorldEditPlugin) {
							WorldEditPlugin worldedit = (WorldEditPlugin) plugin;
							if (arguments.length > 2) {
								String name = arguments[2];
								boolean exists = this.getPlugin().getConfig().contains("public." + name);
								if (arguments[1].equalsIgnoreCase("remove")) {
									if (exists) {
										this.getPlugin().getConfig().set("public." + name, null);
										this.getPlugin().saveConfig();
										sender.sendMessage(ChatColor.GREEN + "La zone a �t� supprim�e.");
									} else sender.sendMessage(ChatColor.RED + "Cette zone n'�xiste pas.");
								} else if (arguments[1].equalsIgnoreCase("add") || arguments[1].equalsIgnoreCase("edit")) {
									if ((!exists && arguments[1].equalsIgnoreCase("add")) || (exists && arguments[1].equalsIgnoreCase("edit"))) try {
										LocalSession session = worldedit.getSession(player);
										Region region = session.getSelection(session.getSelectionWorld());
										Vector first = region.getMinimumPoint();
										Vector second = region.getMaximumPoint();
										this.getPlugin().getConfig().set("public." + name + ".world", player.getWorld().getUID().toString());
										this.getPlugin().getConfig().set("public." + name + ".first.x", first.getX());
										this.getPlugin().getConfig().set("public." + name + ".first.z", first.getZ());
										this.getPlugin().getConfig().set("public." + name + ".second.x", second.getX());
										this.getPlugin().getConfig().set("public." + name + ".second.z", second.getZ());
										this.getPlugin().saveConfig();
										if (exists) sender.sendMessage(ChatColor.GREEN + "La zone a �t� modifi�e."); else sender.sendMessage(ChatColor.GREEN + "La zone a �t� cr�er.");
									} catch (Exception exception) {
										sender.sendMessage(ChatColor.RED + "Votre selection n'est pas compl�te.");
									} else if (exists) sender.sendMessage(ChatColor.RED + "Cette zone �xiste d�j�."); else sender.sendMessage(ChatColor.RED + "Cette zone n'�xiste pas.");
								}
							} else sender.sendMessage(ChatColor.RED + "Renseignez le nom de la zone.");
						} else throw new Exception();
						return true;
					} catch (Exception exception) {
						sender.sendMessage(ChatColor.RED + "WorldEdit n'est pas actif.");
						return true;
					}
					return this.noPermission(sender);
				}
				if (arguments[0].equalsIgnoreCase("add") || arguments[0].equalsIgnoreCase("remove")) {
					if (!(information.getOwner() == null)) {
						if (information.getOwner().equals(player.getUniqueId())) {
							@SuppressWarnings("deprecation")
							OfflinePlayer temporaly = this.getPlugin().getServer().getOfflinePlayer(arguments[1]);
							if (!(temporaly == null) && !(temporaly.equals(player.getUniqueId()))) try {
								if (arguments[0].equalsIgnoreCase("add")) {
									if (!information.getPlayers().contains(temporaly.getUniqueId())) {
										information.addPlayer(this.getPlugin().getDatabase(), temporaly.getUniqueId());
										sender.sendMessage(ChatColor.GREEN + "Joueur ajout� !");
									} else sender.sendMessage(ChatColor.RED + "Ce joueur est d�j� ajout�.");
								} else if (arguments[0].equalsIgnoreCase("remove")) {
									if (!information.getPlayers().contains(temporaly.getUniqueId())) {
										information.removePlayer(this.getPlugin().getDatabase(), temporaly.getUniqueId());
										sender.sendMessage(ChatColor.GREEN + "Joueur supprim� !");
									} else sender.sendMessage(ChatColor.RED + "Ce joueur n'est pas ajout�.");
								}
							} catch (Exception exception) {
								sender.sendMessage(ChatColor.RED + "Impossible de faire cette action pour le moment, contacter l'administration.");
							} else sender.sendMessage(ChatColor.RED + "Impossible d'ex�cuter la commande pour ce joueur.");
						} else sender.sendMessage(ChatColor.RED + "Vous n'�tes pas le propri�taire de la zone.");
					} else sender.sendMessage(ChatColor.RED + "Cette zone n'est pas prot�g�e."); 
				}
			} else {
				if (!ProtectionType.PUBLIC.is(this.getPlugin(), player.getLocation(), null)) {
					ProtectionInformation information = this.getPlugin().getProtection().getChunk(player.getChunk(), true);
					if (information.getOwner() == null) {
						DatabaseLink link = this.getPlugin().getPlayer(player);
						int chunks = link.getData("chunks", new DatabaseData(4)).getAsInteger();
						if (chunks > 0 || player.hasPermission("moderation.protection.unlimited")) try {
							information.setOwner(this.getPlugin().getDatabase(), player.getUniqueId());
							chunks--;
							String message = chunks + " protections restantes";
							if (player.hasPermission("moderation.protection.unlimited")) message = "Protections illimit�s";
							link.setData("chunks", String.valueOf(chunks));
							sender.sendMessage(ChatColor.GREEN + "Zone prot�g�e. " + ChatColor.AQUA + "(" + message + ")");
						} catch (Exception exception) {
							sender.sendMessage(ChatColor.RED + "Impossible de prot�ger cette zone pour le moment, contacter l'administration.");
							exception.printStackTrace();
						} else player.sendRawMessage("[\"\",{\"text\":\"Vous ne disposez plus de protections.\",\"color\":\"red\"},{\"text\":\" \"},{\"text\":\"Vous pouvez obtenir des protections supl�mentaires dans la boutiqueen tapant\",\"color\":\"gray\"},{\"text\":\" \"},{\"text\":\"/shop\",\"color\":\"yellow\",\"clickEvent\":{\"action\":\"run_command\",\"value\":\"/shop\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":[\"\",{\"text\":\"Cliquez ici\",\"color\":\"yellow\"},{\"text\":\" \"},{\"text\":\"pour \",\"color\":\"gray\"},{\"text\":\"ex�cuter la commande\",\"color\":\"yellow\"}]}},{\"text\":\" \"},{\"text\":\"ou sur le site\",\"color\":\"gray\"},{\"text\":\" \"},{\"text\":\"shop.lynecraft.fr\",\"underlined\":true,\"color\":\"blue\",\"clickEvent\":{\"action\":\"open_url\",\"value\":\"shop.lynecraft.fr\"},\"hoverEvent\":{\"action\":\"show_text\",\"value\":[\"\",{\"text\":\"Cliquez ici\",\"color\":\"yellow\"},{\"text\":\" \"},{\"text\":\"pour ouvrir\",\"color\":\"gray\"},{\"text\":\" \"},{\"text\":\"la boutique en ligne\",\"color\":\"yellow\"}]}}]");
					} else sender.sendMessage(ChatColor.RED + "Cette zone est d�j� prot�g�e.");
				} else sender.sendMessage(ChatColor.RED + "Cette zone est publique, impossible de la prot�ger.");
			}
		} else return this.noPermission(sender);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		List<String> temporaly = new ArrayList<String>();
		if (sender instanceof Player) {
			if (arguments.length == 1) {
				temporaly.add("add");
				temporaly.add("remove");
				temporaly.add("list");
				if (sender.hasPermission("moderation.protection.info")) temporaly.add("info");
				if (sender.hasPermission("moderation.protection.public")) temporaly.add("public");
			} else if (arguments.length == 2) {
				if (arguments[0].equalsIgnoreCase("add") || arguments[0].equalsIgnoreCase("remove")) for (Player player : this.getPlugin().getServer().getOnlinePlayers()) temporaly.add(player.getName());
				if (arguments[0].equalsIgnoreCase("public") && sender.hasPermission("moderation.protection.public")) {
					temporaly.add("add");
					temporaly.add("edit");
					temporaly.add("remove");
				}
			} else if (arguments.length == 3 && arguments[0].equalsIgnoreCase("public") && sender.hasPermission("moderation.protection.public") && (arguments[1].equalsIgnoreCase("remove") || arguments[1].equalsIgnoreCase("edit"))) {
				for (String name : this.getPlugin().getConfig().getConfigurationSection("public").getKeys(false)) temporaly.add(name);
			}
		}
		return temporaly;
	}
}