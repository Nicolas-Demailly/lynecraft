package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockBreakEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public class BlockBreak extends Listener {
	public BlockBreak(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void blockBreak(BlockBreakEvent event) {
		if (this.getPlugin().getProtection().isProtected(event.getPlayer(), event.getBlock().getLocation())) event.setCancelled(true);
	}
}