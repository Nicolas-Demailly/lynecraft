package fr.nicolasdemailly.lynecraft.setting;

import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import fr.lyneteam.nico.database.DatabaseData;
import fr.nicolasdemailly.lynecraft.LyneCraft;

public abstract class SettingValue implements Listener {
	private final SettingItem item;
	protected Setting setting;
	protected int value;
	public LyneCraft plugin;
	
	public SettingValue(SettingItem item) {
		this.item = item;
	}
	
	protected int getValue(UUID uuid) {
		return this.plugin.getPlayer(uuid).getData("setting." + this.setting.name().toLowerCase(), new DatabaseData(this.setting.getDefault().value)).getAsInteger();
	}
	
	public final ItemStack getItem(Player player) {
		return this.item.get(player);
	}
	
	public final boolean has(UUID uuid) {
		return getValue(uuid) == this.value;
	}
	
	public final boolean has(Player player) {
		return this.has(player.getUniqueId());
	}
	
	public final boolean set(Player player) {
		SettingValue old = this.setting.getValues()[this.getValue(player.getUniqueId())];
		if (old.disable(player)) if (this.enable(player)) {
			this.plugin.getPlayer(player.getUniqueId()).setData("setting." + this.setting.name().toLowerCase(), String.valueOf(this.value));
			return true;
		} else old.enable(player);
		return false;
	}
	
	public abstract boolean enable(Player player);
	public abstract boolean disable(Player player);
}