package fr.nicolasdemailly.lynecraft.command;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public abstract class Command implements CommandExecutor, TabCompleter  {
	private LyneCraft plugin;
	
	public Command(LyneCraft plugin, String label) {
		this.plugin = plugin;
		PluginCommand command = plugin.getCommand(label);
		command.setExecutor(this);
		command.setTabCompleter(this);
	}
	
	public final LyneCraft getPlugin() {
		return this.plugin;
	}
	
	protected boolean noPermission(CommandSender sender) {
		sender.sendMessage(ChatColor.RED + "Vous n'avez pas les permissions de faire ceci.");
		return true;
	}
}