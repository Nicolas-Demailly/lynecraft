package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerInteractEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public class PlayerInteract extends Listener {
	public PlayerInteract(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void playerInteract(PlayerInteractEvent event) {
		if (event.hasBlock() && !(event.getClickedBlock() == null) && this.getPlugin().getProtection().isProtected(event.getPlayer(), event.getClickedBlock().getLocation())) event.setCancelled(true);
	}
}