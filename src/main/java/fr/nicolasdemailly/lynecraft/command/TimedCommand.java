package fr.nicolasdemailly.lynecraft.command;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public abstract class TimedCommand extends Command {
	private final static SimpleDateFormat formater = new SimpleDateFormat("yyyy-MM-dd HH-mm-ss");
	
	public TimedCommand(LyneCraft plugin, String label) {
		super(plugin, label);
	}
	
	protected String getDate(String value) {
		value = value + "@";
		Calendar calendar = Calendar.getInstance();
		for (Time time : Time.values()) {
			String[] parts = value.split(time.identifier);
			for (int index = 0; index < parts.length - 1; index++) {
				String part = parts[index];
				Pattern pattern = Pattern.compile("[0-9]+$");
				Matcher matcher = pattern.matcher(part);
				if (matcher.find()) calendar.add(time.calendar, Integer.parseInt(matcher.group()));
			}
		}
		return formater.format(calendar.getTime());
	}
	
	private enum Time {
		YEAR("y", "an", Calendar.YEAR, (long) 1000 * (long) 60 * (long) 60 * (long) 24 * (long) 365, -1),
		DAY("d", "jour", Calendar.DAY_OF_YEAR, (long) 1000 * (long) 60 * (long) 60 * (long) 24, 365),
		HOUR("h", "heure", Calendar.HOUR, (long) 1000 * (long) 60 * 60, 24),
		MINUTE("m", "minute", Calendar.MINUTE, (long) 1000 * 60, 60),
		SECOND("s", "seconde", Calendar.SECOND, (long) 1000, 60);
		
		private final String identifier, display;
		private final int calendar;
		private final long multiplier, percent;
		
		private Time(String identifier, String display, int calendar, long multiplier, long percent) {
			this.identifier = identifier;
			this.display = display;
			this.calendar = calendar;
			this.multiplier = multiplier;
			this.percent = percent;
		}
		
		private final String getFormat(long date) {
            long amount = 0;            
			if (this.percent < 0) amount = date / this.multiplier; else amount = (date / this.multiplier) % this.percent;
			if (amount == 0) return ""; else if (amount == 1) return "1 " + this.display; else return amount + " " + this.display + "s";
		}
	}
	
	public static String format(Date value) {
		long date = value.getTime() - new Date().getTime();
		String format = "";
		for (Time time : Time.values()) format += " " + time.getFormat(date);
		format = format.trim().replaceAll(" +", " ");
		if (format.startsWith(" ")) format = format.substring(1);
		return format;
	}
	
	public static Date get(String value) {
		try {
			return formater.parse(value);
		} catch (Exception exception) {
			return new Date();
		}
	}
}