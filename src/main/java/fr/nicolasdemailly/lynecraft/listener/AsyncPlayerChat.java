package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.message.Message;

public class AsyncPlayerChat extends Listener {
	public AsyncPlayerChat(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void asyncPlayerChat(AsyncPlayerChatEvent event) {
		event.setCancelled(true);
		this.getPlugin().broadcastMessage(event.getPlayer(), Message.CHAT, event.getMessage());
	}
}