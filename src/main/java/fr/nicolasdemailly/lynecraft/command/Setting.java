package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.inventory.Inventory;

public class Setting extends Command {
	public Setting(LyneCraft plugin) {
		super(plugin, "setting");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender instanceof Player) {
			Inventory.SETTING.open((Player) sender);
		} else return this.noPermission(sender);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		return new ArrayList<String>();
	}
}