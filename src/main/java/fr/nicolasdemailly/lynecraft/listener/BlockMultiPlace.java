package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockMultiPlaceEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.protection.Protection;

public class BlockMultiPlace extends Listener {
	public BlockMultiPlace(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void blockMultiPlace(BlockMultiPlaceEvent event) {
		Player player = event.getPlayer();
		Protection protection = this.getPlugin().getProtection();
		if (protection.isProtected(player, event.getBlock().getLocation())
				|| protection.isProtected(player, event.getBlockPlaced().getLocation())
				|| protection.isProtected(player, event.getBlockAgainst().getLocation())) event.setCancelled(true);
	}
}