package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.block.BlockPlaceEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public class BlockPlace extends Listener {
	public BlockPlace(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void blockPlace(BlockPlaceEvent event) {
		if (this.getPlugin().getProtection().isProtected(event.getPlayer(), event.getBlock().getLocation())) event.setCancelled(true);
	}
}