package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;

public class Group extends Command {
	public Group(LyneCraft plugin) {
		super(plugin, "group");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender.hasPermission("administration.group")) {
			if (arguments.length > 1) {
				UUID uuid = this.getPlugin().getServer().getPlayerUniqueId(arguments[0]);
				if (!(uuid == null)) try {
					String name = arguments[1];
					for (fr.nicolasdemailly.lynecraft.group.Group group : fr.nicolasdemailly.lynecraft.group.Group.values()) {
						if (group.getName().equalsIgnoreCase(name)) {
							DatabaseLink link = this.getPlugin().getOfflinePlayer(uuid);
							link.setData("group", group.name());
							link.saveDatabaseData();
							Player player = this.getPlugin().getServer().getPlayer(uuid);
							if (!(player == null)) group.setup(player);
							sender.sendMessage(ChatColor.GREEN + "Le groupe de ce joueur a �t� changer.");
							return true;
						}
					}
					sender.sendMessage(ChatColor.RED + "Ce groupe n'�xiste pas.");
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				sender.sendMessage(ChatColor.RED + "Impossible de changer le groupe de ce joueur.");
				return true;
			}
		}
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		List<String> temporaly = new ArrayList<String>();
		if (arguments.length == 1) for (Player player : this.getPlugin().getServer().getOnlinePlayers()) temporaly.add(player.getName());
		if (arguments.length == 2) for (fr.nicolasdemailly.lynecraft.group.Group group : fr.nicolasdemailly.lynecraft.group.Group.values()) temporaly.add(group.getName());
		return temporaly;
	}
}