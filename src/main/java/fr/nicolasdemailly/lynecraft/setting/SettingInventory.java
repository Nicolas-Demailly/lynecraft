package fr.nicolasdemailly.lynecraft.setting;

import java.util.List;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;

import fr.nicolasdemailly.lynecraft.inventory.OpenableInventory;

public class SettingInventory extends OpenableInventory {
	@Override
	public void open(Player player) {
		List<Setting> settings = Setting.get(player);
		int size = 9;
		while (size < settings.size()) size += 9;
		Inventory inventory = this.plugin.getServer().createInventory(player, size, "Paramètres");
		for (int index = 0; index < settings.size(); index++) {
			Setting setting = settings.get(index);
			for (SettingValue value : setting.getValues()) if (value.has(player)) inventory.setItem(index, value.getItem(player));
		}
		player.openInventory(inventory);
	}
	
	@EventHandler
	public void inventoryInteract(InventoryInteractEvent event) {
		if (event.getInventory().getName().equals("Paramètres")) event.setCancelled(true);
	}
	
	@EventHandler
	public void inventoryClick(InventoryClickEvent event) {
		if (event.getInventory().getName().equals("Paramètres")) {
			event.setCancelled(true);
			Player player = (Player) event.getWhoClicked();
			int index = 0;
			for (Setting setting : Setting.values()) {
				if (setting.getPermission() == null || player.hasPermission(setting.getPermission())) {
					if (index == event.getSlot()) {
						for (int temporaly = 0; temporaly < setting.getValues().length; temporaly++) {
							if (setting.getValues()[temporaly].has(player)) {
								temporaly++;
								if (temporaly == setting.getValues().length) temporaly = 0;
								SettingValue value = setting.getValues()[temporaly];
								if (value.set(player)) {
									event.getInventory().setItem(event.getSlot(), value.getItem(player));
									player.playSound(player.getLocation(), Sound.BLOCK_NOTE_BLOCK_PLING, 1, 1);
								} else player.playSound(player.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_WOODEN_DOOR, 1, 1);
								return;
							}
						}
					}
					index++;
				}
			}
		}
	}
}