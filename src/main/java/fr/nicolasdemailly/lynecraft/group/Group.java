package fr.nicolasdemailly.lynecraft.group;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

public enum Group {
	DEFAULT("Aucun", new GroupMethod() {
		
		@Override
		public void setup(Player player) {
			player.setPlayerListName(ChatColor.GRAY + player.getName());
		}
		
		@Override
		public final String chat(Player player, String message) {
			return String.valueOf(ChatColor.GRAY) + player.getName() + " : " + message.replace(String.valueOf(ChatColor.RESET), String.valueOf(ChatColor.GRAY));
		}
		
	}, "dummy.dummy"),
	VIP("V.I.P", new GroupMethod() {
		
		@Override
		public void setup(Player player) {
			player.setPlayerListName(ChatColor.DARK_PURPLE + "[" + ChatColor.LIGHT_PURPLE + "VIP" + ChatColor.DARK_PURPLE + "] " + ChatColor.LIGHT_PURPLE + player.getName());
		}
		
		@Override
		public final String chat(Player player, String message) {
			return ChatColor.DARK_PURPLE + "[" + ChatColor.LIGHT_PURPLE + "VIP" + ChatColor.DARK_PURPLE + "] " + ChatColor.LIGHT_PURPLE + player.getName() + ChatColor.GRAY + " : " + message.replace(String.valueOf(ChatColor.RESET), String.valueOf(ChatColor.GRAY));
		}
		
	}, "dummy.dummy"),
	BUILDER("Constructeur", new GroupMethod() {
		
		@Override
		public void setup(Player player) {
			player.setPlayerListName(ChatColor.GOLD + "[" + ChatColor.YELLOW + "Constructeur" + ChatColor.GOLD + "] " + ChatColor.YELLOW + player.getName());
		}
		
		@Override
		public final String chat(Player player, String message) {
			return ChatColor.GOLD + "[" + ChatColor.YELLOW + "Constructeur" + ChatColor.GOLD + "] " + ChatColor.YELLOW + player.getName() + ChatColor.GOLD + " : " + ChatColor.YELLOW + message.replace(String.valueOf(ChatColor.RESET), String.valueOf(ChatColor.YELLOW));
		}
		
	}, "dummy.dummy"),
	HELPER("Helper", new GroupMethod() {
		
		@Override
		public void setup(Player player) {
			player.setPlayerListName(ChatColor.GRAY + "[" + ChatColor.WHITE + "Helper" + ChatColor.GRAY + "] " + ChatColor.WHITE + player.getName());
		}
		
		@Override
		public final String chat(Player player, String message) {
			return ChatColor.GRAY + "[" + ChatColor.WHITE + "Helper" + ChatColor.GRAY + "] " + ChatColor.WHITE + player.getName() + ChatColor.GRAY + " : " + ChatColor.WHITE + message.replace(String.valueOf(ChatColor.RESET), String.valueOf(ChatColor.WHITE));
		}
		
	}, "dummy.dummy"),
	MODERATOR("Modérateur", new GroupMethod() {

		@Override
		public void setup(Player player) {
			player.setPlayerListName(ChatColor.DARK_AQUA + "[" + ChatColor.AQUA + "Modérateur" + ChatColor.DARK_AQUA + "] " + ChatColor.AQUA + player.getName());
		}
		
		@Override
		public final String chat(Player player, String message) {
			return ChatColor.DARK_AQUA + "[" + ChatColor.AQUA + "Modérateur" + ChatColor.DARK_AQUA + "] " + ChatColor.AQUA + player.getName() + ChatColor.DARK_AQUA + " : " + ChatColor.AQUA + message.replace(String.valueOf(ChatColor.RESET), String.valueOf(ChatColor.AQUA));
		}
		
	}, "dummy.dummy"),
	ADMINISTRATOR("Administrateur", new GroupMethod() {

		@Override
		public void setup(Player player) {
			player.setPlayerListName(ChatColor.DARK_GREEN + "[" + ChatColor.GREEN + "Administrateur" + ChatColor.DARK_GREEN + "] " + ChatColor.GREEN + player.getName());
		}
		
		@Override
		public final String chat(Player player, String message) {
			return ChatColor.DARK_GREEN + "[" + ChatColor.GREEN + "Administrateur" + ChatColor.DARK_GREEN + "] " + ChatColor.GREEN + player.getName() + ChatColor.DARK_GREEN + " : " + ChatColor.GREEN + message.replace(String.valueOf(ChatColor.RESET), String.valueOf(ChatColor.GREEN));
		}
		
	}, "moderation.vanish", "build.gamemode", "administration.reset", ".*", "*");
	
	private final String name;
	private final GroupMethod method;
	private final String[] permissions;
	
	private Group(String name, GroupMethod method, String... permissions) {
		this.name = name;
		this.method = method;
		this.permissions = permissions;
	}

	public final String getName() {
		return this.name;
	}

	public final GroupMethod getMethod() {
		return this.method;
	}

	public void setup(Player player) {
		this.method.plugin.setPermissions(player, this.permissions);
		this.method.setup(player);
	}

	public String chat(Player player, String message) {
		return this.method.chat(player, message);
	}
}