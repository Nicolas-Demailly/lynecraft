package fr.nicolasdemailly.lynecraft.setting;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.World;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerChangedWorldEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.inventory.ItemFlag;

import fr.nicolasdemailly.lynecraft.message.Message;

public enum Setting {
	DIFFICULTY(null, new SettingValue(new SettingItem() {
		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.YELLOW_CONCRETE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Difficult� : " + ChatColor.YELLOW + "Moyenne");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Difficult� de base, aucunes");
			lore.add(ChatColor.GRAY + "modifications au gameplay");
			lore.add(ChatColor.GRAY + "n'a �t� apport�");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {
		
		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.ORANGE_CONCRETE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Difficult� : " + ChatColor.RED + "Difficile");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Difficult� sup�rieure, vous");
			lore.add(ChatColor.GRAY + "ne reg�n�rez plus votre vie.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

		@EventHandler
		public void entityRegainHealth(EntityRegainHealthEvent event) {
			if (event.getEntity() instanceof Player && this.has((Player) event.getEntity()))
				event.setCancelled(true);
		}

	}, new SettingValue(new SettingItem() {
		
		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.RED_CONCRETE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Difficult� : " + ChatColor.DARK_RED + "EXTR�ME");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Difficult� accrue, vous ne");
			lore.add(ChatColor.GRAY + "reg�n�rez plus votre vie et");
			lore.add(ChatColor.GRAY + "tout les d�gats subis sont");
			lore.add(ChatColor.GRAY + "multipli�s par trois.");
			lore.add(" ");
			lore.add(ChatColor.RED + "Les qu�tes rapporent deux fois");
			lore.add(ChatColor.RED + "plus d'argent.");
			lore.add(ChatColor.RED + "D�conseill� en PvP.");
			lore.add(" ");
			lore.add(ChatColor.YELLOW + String.valueOf(ChatColor.ITALIC) + "Pour Bary94...");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

		@EventHandler
		public void entityRegainHealth(EntityRegainHealthEvent event) {
			if (event.getEntity() instanceof Player && this.has((Player) event.getEntity()))
				event.setCancelled(true);
		}
		
		@EventHandler
		public void entityDamage(EntityDamageEvent event) {
			if (event.getEntity() instanceof Player && this.has((Player) event.getEntity())) event.setDamage(event.getDamage() * 3);
		}

	}, new SettingValue(new SettingItem() {
		
		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.LIME_CONCRETE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Difficult� : " + ChatColor.GREEN + "Facile");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Difficult� inexistante, le");
			lore.add(ChatColor.GRAY + "syst�me de combat est identique");
			lore.add(ChatColor.GRAY + "� la version 1.8.");
			lore.add("");
			lore.add(ChatColor.RED + "Vous ne pouvez pas activer");
			lore.add(ChatColor.RED + "l'option PvP.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			if (PVP.getValues()[1].has(player)) {
				player.sendMessage(ChatColor.RED + "D�sactivez d'abord le PvP.");
				return false;
			} else {
				player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(4.0D);
				player.saveData();
				return true;
			}
		}

		@Override
		public boolean disable(Player player) {
			player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).getDefaultValue());
			player.saveData();
			return true;
		}
		
		@EventHandler
		public void onWorldChange(PlayerChangedWorldEvent event) {
			if (this.has(event.getPlayer())) {
				event.getPlayer().getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(4.0D);
				event.getPlayer().saveData();
			}
		}

	}), PVP(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.WOODEN_SWORD);
			item.getItemFlags().add(ItemFlag.HIDE_ATTRIBUTES);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "PvP : " + ChatColor.RED + "Innactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Personne ne peut vous combattre,");
			lore.add(ChatColor.GRAY + "vous ne pouvez combattre personne.");
			lore.add("");
			lore.add(ChatColor.RED + "Vous ne pouvez pas avoir le");
			lore.add(ChatColor.RED + "PvP avec l'option de difficult�");
			lore.add(ChatColor.RED + "facile.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

		@EventHandler
		public void entityDamageByEntity(EntityDamageByEntityEvent event) {
			if (event.getEntity() instanceof Player) {
				Player player = (Player) event.getEntity();
				if (event.getDamager() instanceof Player || (event.getDamager() instanceof Projectile && ((Projectile) event.getDamager()).getShooter() instanceof Player)) {
					Player damager = (event.getDamager() instanceof Player) ? (Player) event.getDamager() : (Player) ((Projectile) event.getDamager()).getShooter();
					if (this.has(damager)) {
						damager.sendActionBar(ChatColor.RED + "Ce joueur n'a pas activer l'option PvP dans le menu " + ChatColor.DARK_AQUA + "/options");
						damager.playSound(damager.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_WOODEN_DOOR, 1, 1);
						event.setCancelled(true);
					}
					if (this.has(player)) {
						player.sendActionBar(ChatColor.RED + "Votre option PvP est d�sactiv� dans le menu " + ChatColor.DARK_AQUA + "/options");
						player.playSound(damager.getLocation(), Sound.ENTITY_ZOMBIE_ATTACK_WOODEN_DOOR, 1, 1);
						event.setCancelled(true);
					}
				}
			}
		}
		
	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.DIAMOND_SWORD);
			item.getItemFlags().add(ItemFlag.HIDE_ATTRIBUTES);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "PvP : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous pouvez combattre les joueurs");
			lore.add(ChatColor.GRAY + "ayant activ� cette option.");
			lore.add("");
			lore.add(ChatColor.RED + "Vous ne pouvez pas avoir le");
			lore.add(ChatColor.RED + "PvP avec l'option de difficult�");
			lore.add(ChatColor.RED + "facile.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			if (DIFFICULTY.getValues()[2].has(player)) {
				player.sendMessage(ChatColor.RED + "Changer d'abord de mode de difficult�.");
				return false;
			} else return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}), SCOREBOARD(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.GREEN_STAINED_GLASS_PANE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Scoreboard : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Le panneau d'information est affich�.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {	
		@Override
		public boolean enable(Player player) {
			player.getScoreboard().getObjectives().iterator().next().setDisplaySlot(DisplaySlot.SIDEBAR);
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.RED_STAINED_GLASS_PANE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Scoreboard : " + ChatColor.RED + "Inactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Le panneau d'information est masqu�.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			player.getScoreboard().clearSlot(DisplaySlot.SIDEBAR);
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}), CHAT(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.WRITABLE_BOOK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Chat : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Le chat est affich�.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.BOOK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Chat : " + ChatColor.RED + "Inactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Le chat est affich�.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}), PRIVATE(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.PAPER);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Messages priv�s : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous pouvez envoyer et recevoir");
			lore.add(ChatColor.GRAY + "des messages priv�s.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.MAP);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Messages priv�s : " + ChatColor.RED + "Inactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous ne pouvez ni envoyer, ni");
			lore.add(ChatColor.GRAY + "recevoir de messages priv�s.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}), LOG(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.SKELETON_SKULL);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Messages de connexion : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous voyez les messages de");
			lore.add(ChatColor.GRAY + "connexion et de d�connexion.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.SKELETON_SKULL);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Messages de connexion : " + ChatColor.RED + "Inactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Les messages de connexion et");
			lore.add(ChatColor.GRAY + "de d�connexion sont masqu�s.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}), DEATH(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.SKELETON_SKULL);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Messages de mort : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous voyez les messages de");
			lore.add(ChatColor.GRAY + "mort.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.SKELETON_SKULL);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Messages de mort : " + ChatColor.RED + "Inactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Les messages de mort sont");
			lore.add(ChatColor.GRAY + "masqu�s.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}
		
		@EventHandler
		public void playerDeath(PlayerDeathEvent event) {
			this.plugin.broadcastMessage(null, Message.DEATH, event.getDeathMessage());
			event.setDeathMessage(null);
		}

	}), SOUND(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.JUKEBOX);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Alerte sonore : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Un petit son retentira lorsqu'un");
			lore.add(ChatColor.GRAY + "joueur fera mention de votre");
			lore.add(ChatColor.GRAY + "pseudonyme dans le chat.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.NOTE_BLOCK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Alerte sonore : " + ChatColor.RED + "Inactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "L'alerte sonore est d�sactiv�.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}), VIEW(null, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.IRON_CHESTPLATE);
			item.getItemFlags().add(ItemFlag.HIDE_PLACED_ON);
			item.getItemFlags().add(ItemFlag.HIDE_PLACED_ON);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Visibilit� des joueurs : " + ChatColor.GREEN + "Actif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous voyez les joueurs.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.LEATHER_CHESTPLATE);
			item.getItemFlags().add(ItemFlag.HIDE_PLACED_ON);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Visibilit� des joueurs : " + ChatColor.RED + "Inactif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous ne voyez aucun joueur.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}), GAMEMODE("build.gamemode", new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.IRON_BLOCK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Mode de jeu : " + ChatColor.WHITE + "Survie");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Mode de jeu survie.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		protected int getValue(UUID uuid) {
			switch (this.plugin.getServer().getPlayer(uuid).getGameMode()) {
				case SURVIVAL: return 0;
				case CREATIVE: return 1;
				case SPECTATOR: return 2;
				case ADVENTURE: return 3;
			}
			return -1;
		}

		@Override
		public boolean enable(Player player) {
			player.setGameMode(GameMode.SURVIVAL);
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.EMERALD_BLOCK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Mode de jeu : " + ChatColor.GREEN + "Cr�atif");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Mode de jeu cr�atif.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		protected int getValue(UUID uuid) {
			switch (this.plugin.getServer().getPlayer(uuid).getGameMode()) {
				case SURVIVAL: return 0;
				case CREATIVE: return 1;
				case SPECTATOR: return 2;
				case ADVENTURE: return 3;
			}
			return -1;
		}

		@Override
		public boolean enable(Player player) {
			player.setGameMode(GameMode.CREATIVE);
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.GOLD_BLOCK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Mode de jeu : " + ChatColor.YELLOW + "Spectateur");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Mode de jeu spectateur.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		protected int getValue(UUID uuid) {
			switch (this.plugin.getServer().getPlayer(uuid).getGameMode()) {
				case SURVIVAL: return 0;
				case CREATIVE: return 1;
				case SPECTATOR: return 2;
				case ADVENTURE: return 3;
			}
			return -1;
		}
		
		@Override
		public boolean enable(Player player) {
			player.setGameMode(GameMode.SPECTATOR);
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.REDSTONE_BLOCK);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Mode de jeu : " + ChatColor.RED + "Aventure");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Mode de jeu aventure.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		protected int getValue(UUID uuid) {
			switch (this.plugin.getServer().getPlayer(uuid).getGameMode()) {
				case SURVIVAL: return 0;
				case CREATIVE: return 1;
				case SPECTATOR: return 2;
				case ADVENTURE: return 3;
			}
			return -1;
		}
		
		@Override
		public boolean enable(Player player) {
			player.setGameMode(GameMode.ADVENTURE);
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}),
	VANISH("moderation.vanish", new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.DIAMOND_CHESTPLATE);
			item.getItemFlags().add(ItemFlag.HIDE_PLACED_ON);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Invisibilit� : " + ChatColor.RED + "Inactive");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Les autres joueurs vous voyent.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.CHAINMAIL_CHESTPLATE);
			item.getItemFlags().add(ItemFlag.HIDE_PLACED_ON);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Invisibilit� : " + ChatColor.GREEN + "Active");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Les joueurs ne poss�dant pas");
			lore.add(ChatColor.GRAY + "ce param�tre ne vous voyent pas.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@SuppressWarnings("deprecation")
		@Override
		public boolean enable(Player player) {
			for (Player other : this.plugin.getServer().getOnlinePlayers()) if (!other.hasPermission("moderation.vanish")) other.hidePlayer(player);
			return true;
		}

		@SuppressWarnings("deprecation")
		@Override
		public boolean disable(Player player) {
			for (Player other : this.plugin.getServer().getOnlinePlayers()) if (VIEW.getDefault().has(other)) other.showPlayer(player);
			return true;
		}

	}),
	INVINCIBILITY("administration.invincibility", new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.APPLE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Invincibilit� : " + ChatColor.RED + "Inactive");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous �tes mortel.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.GOLDEN_APPLE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Invincibilit� : " + ChatColor.GREEN + "Active");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous �tes invisible mais");
			lore.add(ChatColor.GRAY + "vous prennez les coups.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}
		
		@EventHandler
		public void entityDamage(EntityDamageEvent event) {
			if (event.getEntity() instanceof Player && this.has((Player) event.getEntity())) event.setDamage(0);
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.ENCHANTED_GOLDEN_APPLE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Invincibilit� : " + ChatColor.GOLD + "Dieu");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Vous �tes invisible et vous");
			lore.add(ChatColor.GRAY + "ne sentez rien.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			return true;
		}
		
		@EventHandler
		public void entityDamage(EntityDamageEvent event) {
			if (event.getEntity() instanceof Player && this.has((Player) event.getEntity())) event.setCancelled(true);
		}

	}),
	WEATHER("administration.weather", new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.SUNFLOWER);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Temps : " + ChatColor.YELLOW + "Normal");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Le temps est normal.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		protected int getValue(UUID uuid) {
			World world = this.plugin.getServer().getPlayer(uuid).getWorld();
			if (world.isThundering() && world.hasStorm()) return 2;
			if (world.hasStorm()) return 1;
			return 0;
		}

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			World world = player.getWorld();
			world.setThunderDuration(0);
			world.setThundering(false);
			world.setStorm(true);
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.POTION);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Temps : " + ChatColor.WHITE + "Pluvieux");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Le temps est pluvieux.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		protected int getValue(UUID uuid) {
			World world = this.plugin.getServer().getPlayer(uuid).getWorld();
			if (world.isThundering() && world.hasStorm()) return 2;
			if (world.hasStorm()) return 1;
			return 0;
		}

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			World world = player.getWorld();
			world.setStorm(true);
			world.setThundering(true);
			world.setThunderDuration(world.getWeatherDuration());
			return true;
		}
		
		@EventHandler
		public void entityDamage(EntityDamageEvent event) {
			if (event.getEntity() instanceof Player && this.has((Player) event.getEntity())) event.setDamage(0);
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.TRIDENT);
			item.getItemFlags().add(ItemFlag.HIDE_ATTRIBUTES);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Temps : " + ChatColor.DARK_AQUA + "Orageux");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Le temps est orageux.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		protected int getValue(UUID uuid) {
			World world = this.plugin.getServer().getPlayer(uuid).getWorld();
			if (world.isThundering() && world.hasStorm()) return 2;
			if (world.hasStorm()) return 1;
			return 0;
		}

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			World world = player.getWorld();
			world.setThunderDuration(0);
			world.setThundering(false);
			world.setStorm(false);
			return true;
		}

	}),
	TIME("administration.time", new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Heure : " + ChatColor.YELLOW + "Jour");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Il fait jour.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		protected int getValue(UUID uuid) {
			long time = this.plugin.getServer().getPlayer(uuid).getWorld().getTime();
			if (time > 23000 || time < 13000) return 0;
			return 1;
		}

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			player.getWorld().setTime(13000);
			return true;
		}

	}, new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.BLACK_STAINED_GLASS_PANE);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Heure : " + ChatColor.GRAY + "Nuit");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Il fait nuit.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {
		
		@Override
		protected int getValue(UUID uuid) {
			long time = this.plugin.getServer().getPlayer(uuid).getWorld().getTime();
			if (time > 23000 || time < 13000) return 0;
			return 1;
		}

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			player.getWorld().setTime(1000);
			return true;
		}
		
		@EventHandler
		public void entityDamage(EntityDamageEvent event) {
			if (event.getEntity() instanceof Player && this.has((Player) event.getEntity())) event.setDamage(0);
		}

	}),
	RESET("administration.reset", new SettingValue(new SettingItem() {

		@Override
		public ItemStack get(Player player) {
			ItemStack item = new ItemStack(Material.BARRIER);
			ItemMeta meta = item.getItemMeta();
			meta.setDisplayName(ChatColor.GOLD + "Red�marrage");
			List<String> lore = new ArrayList<String>();
			lore.add(ChatColor.GRAY + "Red�marrer le serveur.");
			meta.setLore(lore);
			item.setItemMeta(meta);
			return item;
		}
		
	}) {

		@Override
		public boolean enable(Player player) {
			return true;
		}

		@Override
		public boolean disable(Player player) {
			this.plugin.getServer().shutdown();
			return true;
		}

	});

	private final String permission;
	private final SettingValue[] values;

	private Setting(String permission, SettingValue... values) {
		this.permission = permission;
		for (int index = 0; index < values.length; index++) {
			SettingValue value = values[index];
			value.setting = this;
			value.value = index;
		}
		this.values = values;
	}

	public final String getPermission() {
		return this.permission;
	}

	public final SettingValue getDefault() {
		return this.values[0];
	}

	public final SettingValue[] getValues() {
		return this.values;
	}

	public void enable(Player player) {
		for (SettingValue value : this.values) if (value.has(player)) {
			value.enable(player);
			return;
		}
	}

	public static List<Setting> get(Player player) {
		List<Setting> settings = new ArrayList<Setting>();
		for (Setting setting : values()) if (setting.permission == null || player.hasPermission(setting.permission)) settings.add(setting);
		return settings;
	}
}