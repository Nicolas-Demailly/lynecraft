package fr.nicolasdemailly.lynecraft.listener;

import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.player.KickedFromServerEvent;
import com.velocitypowered.api.event.player.KickedFromServerEvent.DisconnectPlayer;

import net.kyori.text.Component;
import net.kyori.text.TextComponent;
import net.kyori.text.format.TextColor;

public class KickedFromServer {
	@Subscribe
	public void playerJoin(KickedFromServerEvent event) {
		Component disconnected = TextComponent.of("Vous avez �t� d�connect�.", TextColor.RED);
		if (event.getOriginalReason().isPresent()) disconnected = event.getOriginalReason().get();
		event.setResult(DisconnectPlayer.create(disconnected));
	}
}