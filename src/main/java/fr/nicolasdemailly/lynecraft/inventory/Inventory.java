package fr.nicolasdemailly.lynecraft.inventory;

import org.bukkit.entity.Player;

import fr.nicolasdemailly.lynecraft.setting.SettingInventory;
import fr.nicolasdemailly.lynecraft.protection.ProtectionInventory;

public enum Inventory {
	SETTING(new SettingInventory()),
	PROTECTION(new ProtectionInventory());
	
	private final OpenableInventory inventory;
	
	private Inventory(OpenableInventory inventory) {
		this.inventory = inventory;
	}
	
	public final OpenableInventory getInventory() {
		return this.inventory;
	}

	public void open(Player player) {
		this.inventory.open(player);
	}
}