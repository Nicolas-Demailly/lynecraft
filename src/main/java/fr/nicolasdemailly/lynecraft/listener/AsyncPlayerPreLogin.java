package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public class AsyncPlayerPreLogin extends Listener {
	public AsyncPlayerPreLogin(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void asyncPlayerPreLogin(AsyncPlayerPreLoginEvent event) {
		if (!this.getPlugin().loginPlayer(event.getUniqueId())) {
			event.setLoginResult(Result.KICK_OTHER);
			event.setKickMessage(ChatColor.RED + "Une erreure est survenue, reconnectez-vous.");
		}
		if (!event.getLoginResult().equals(Result.ALLOWED)) this.getPlugin().removePlayer(event.getUniqueId());
	}
}