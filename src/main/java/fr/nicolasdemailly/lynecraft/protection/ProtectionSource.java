package fr.nicolasdemailly.lynecraft.protection;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import fr.lyneteam.nico.database.Database;

public class ProtectionSource {
	private final String t, w;
	private final int x, y, z;
	protected final List<UUID> players;
	protected UUID owner;
	
	public ProtectionSource(String t, String w, int x, int y, int z) {
		this.t = t;
		this.w = w;
		this.x = x;
		this.y = y;
		this.z = z;
		this.players = new ArrayList<UUID>();
	}
	
	public final UUID getOwner() {
		return this.owner;
	}
	
	public final List<UUID> getPlayers() {
		return this.players;
	}

	public boolean isProtected(UUID uuid) {
		return !(this.owner == null || this.players.contains(uuid));
	}
	
	public void addPlayer(Database database, UUID uuid) throws Exception {
		if (this.y == -1) {
			database.execute("INSERT INTO " + this.t + "(w, x, z, o, p) VALUES('" + this.w + "', " + this.x + ", " + this.z + ", 0, '" + uuid.toString() + "')");
		} else {
			database.execute("INSERT INTO " + this.t + "(w, x, y, z, o, p) VALUES('" + this.w + "', " + this.x + ", " + this.y + ", " + this.z + ", 0, '" + uuid.toString() + "')");
		}
		this.players.add(uuid);
	}
	
	public void removePlayer(Database database, UUID uuid) throws Exception {
		if (this.y == -1) {
			database.execute("DELETE FROM " + this.t + " WHERE w='" + this.w + "' AND x=" + this.x + " AND z=" + this.z + " AND p='" + uuid.toString() + "'");
		} else {
			database.execute("DELETE FROM " + this.t + " WHERE w='" + this.w + "' AND x=" + this.x + " AND y=" + this.y + " AND z=" + this.z + " AND p='" + uuid.toString() + "'");
		}
		this.players.remove(uuid);
	}
	
	public void setOwner(Database database, UUID uuid) throws Exception {
		this.removePlayer(database, uuid);
		if (this.y == -1) {
			database.execute("INSERT INTO " + this.t + "(w, x, z, o, p) VALUES('" + this.w + "', " + this.x + ", " + this.z + ", 1, '" + uuid.toString() + "')");
		} else {
			database.execute("INSERT INTO " + this.t + "(w, x, y, z, o, p) VALUES('" + this.w + "', " + this.x + ", " + this.y + ", " + this.z + ", 1, '" + uuid.toString() + "')");
		}
		this.owner = uuid;
		this.players.add(uuid);
	}
	
	public void remove(Database database) throws Exception {
		if (this.y == -1) {
			database.execute("DELETE FROM " + this.t + " WHERE w='" + this.w + "' AND x=" + this.x + " AND z=" + this.z);
		} else {
			database.execute("DELETE FROM " + this.t + " WHERE w='" + this.w + "' AND x=" + this.x + " AND y=" + this.y + " AND z=" + this.z);
		}
		this.players.clear();
		this.owner = null;
	}
}