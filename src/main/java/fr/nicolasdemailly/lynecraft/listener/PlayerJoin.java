package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.message.Message;
import fr.nicolasdemailly.lynecraft.setting.Setting;

public class PlayerJoin extends Listener {
	public PlayerJoin(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void playerJoin(PlayerJoinEvent event) {
		event.setJoinMessage(null);
		DatabaseLink link = this.getPlugin().getPlayer(event.getPlayer());
		if (link == null) event.getPlayer().kickPlayer(ChatColor.RED + "Une erreure est survenue, reconnectez-vous.");
		event.getPlayer().setScoreboard(this.getPlugin().getScoreboard(event.getPlayer()));
		this.getPlugin().getGroup(event.getPlayer()).setup(event.getPlayer());
		for (Setting setting : Setting.values()) setting.enable(event.getPlayer());
		if (link.isEmpty()) {
			this.getPlugin().broadcastMessage(event.getPlayer(), Message.BROADCAST, ChatColor.LIGHT_PURPLE + "" + ChatColor.MAGIC + "|||||" + ChatColor.GREEN + " Bienvenue � " + ChatColor.GOLD + event.getPlayer().getName() + ChatColor.GREEN + " sur " + ChatColor.GOLD + "Lyne-Craft" + ChatColor.GREEN + " ! " + ChatColor.LIGHT_PURPLE + ChatColor.MAGIC + "|||||");
			int total = this.getPlugin().getConfig().getInt("total") + 1;
			this.getPlugin().getConfig().set("total", total);
			this.getPlugin().saveConfig();
			for (Player player : this.getPlugin().getServer().getOnlinePlayers()) player.getScoreboard().getScores(ChatColor.GREEN + "au total").iterator().next().setScore(total);
		} else this.getPlugin().broadcastMessage(event.getPlayer(), Message.LOG, ChatColor.GRAY + event.getPlayer().getName() + " s'est connect�.");
		link.setData("version", "1.14-R0");
		link.setData("name", event.getPlayer().getName());
	}
}