package fr.nicolasdemailly.lynecraft.inventory;

import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public abstract class OpenableInventory implements Listener {
	public LyneCraft plugin;
	
	public abstract void open(Player player);
}