package fr.nicolasdemailly.lynecraft;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.RenderType;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import fr.lyneteam.nico.database.Database;
import fr.lyneteam.nico.database.DatabaseConfiguration;
import fr.lyneteam.nico.database.DatabaseData;
import fr.lyneteam.nico.database.DatabaseLink;
import fr.lyneteam.nico.database.DatabaseUser;
import fr.nicolasdemailly.lynecraft.command.*;
import fr.nicolasdemailly.lynecraft.group.Group;
import fr.nicolasdemailly.lynecraft.inventory.Inventory;
import fr.nicolasdemailly.lynecraft.inventory.OpenableInventory;
import fr.nicolasdemailly.lynecraft.listener.*;
import fr.nicolasdemailly.lynecraft.message.Message;
import fr.nicolasdemailly.lynecraft.protection.Protection;
import fr.nicolasdemailly.lynecraft.setting.Setting;
import fr.nicolasdemailly.lynecraft.setting.SettingValue;

public class LyneCraft extends JavaPlugin implements DatabaseUser {
	private Database database;
	private final Map<UUID, DatabaseLink> players;
	private final Map<UUID, PermissionAttachment> permissions;
	private int entities;
	private Protection protection;
	
	public LyneCraft() {
		try {
			this.database = new DatabaseConfiguration(new File("database.yml")).getDatabase(this);
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		this.players = new HashMap<UUID, DatabaseLink>();
		this.permissions = new HashMap<UUID, PermissionAttachment>();
	}
	
	public void onEnable() {
		new AsyncPlayerChat(this);
		new AsyncPlayerPreLogin(this);
		new BlockBreak(this);
		new BlockMultiPlace(this);
		new BlockPlace(this);
		new PlayerBucketEmpty(this);
		new PlayerInteract(this);
		new PlayerJoin(this);
		new PlayerLogin(this);
		new PlayerQuit(this);
		
		new Ban(this);
		new fr.nicolasdemailly.lynecraft.command.Group(this);
		new Kick(this);
		new Mute(this);
		new Protect(this);
		new fr.nicolasdemailly.lynecraft.command.Setting(this);
		new Unban(this);
		new Unmute(this);
		new Unprotect(this);
		new Test(this);
				
		for (Group group : Group.values()) group.getMethod().plugin = this;
		for (Inventory inventory : Inventory.values()) {
			OpenableInventory openable = inventory.getInventory();
			openable.plugin = this;
			this.getServer().getPluginManager().registerEvents(openable, this);
		}
		for (Setting setting : Setting.values()) for (SettingValue value : setting.getValues()) {
			value.plugin = this;
			this.getServer().getPluginManager().registerEvents(value, this);
		}
		
		this.saveDefaultConfig();
		
		this.protection = new Protection(this);
		
		new Thread() {
			public void run() {
				while (true) try {
					Thread.sleep(2000);
					int entities = 0;
					for (World world : LyneCraft.this.getServer().getWorlds()) entities += world.getEntities().size();
					LyneCraft.this.entities = entities;
					for (Player player : LyneCraft.this.getServer().getOnlinePlayers()) try {
						player.getScoreboard().getScores(ChatColor.GREEN + "Entités").iterator().next().setScore(entities);
					} catch (Exception exception) {
						// Ignore
					}
				} catch (Exception exception) {
					// Ignore
				}
			}
		}.start();
	}
	
	public void onDisable() {
		for (Player player : this.getServer().getOnlinePlayers()) {
			DatabaseLink link = this.getPlayer(player);
			try {
				link.saveDatabaseData();
			} catch (Exception exception) {
				System.out.println("Data of " + player.getUniqueId().toString());
				link.displayData();
				exception.printStackTrace();
			}
			player.kickPlayer(ChatColor.GOLD + "" + ChatColor.MAGIC + "|||||" + ChatColor.RED + " Redémarrage en cours " + ChatColor.GOLD + ChatColor.MAGIC + "|||||");
		}
	}

	@Override
	public void setDatabase(Database database) {
		try {
			database.createTables("players");
			database.execute("CREATE TABLE IF NOT EXISTS `chunks` (`w` VARCHAR(36) NOT NULL, `x` INT NOT NULL, `z` INT NOT NULL, `o` BOOLEAN NOT NULL, `p` VARCHAR(36) NOT NULL);");
			database.execute("CREATE TABLE IF NOT EXISTS `blocks` (`w` VARCHAR(36) NOT NULL, `x` INT NOT NULL, `y` INT NOT NULL, `z` INT NOT NULL, `o` BOOLEAN NOT NULL, `p` VARCHAR(36) NOT NULL);");
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		this.database = database;
	}
	
	public final Database getDatabase() {
		return this.database;
	}
	
	public final boolean loginPlayer(UUID uuid) {
		if (this.players.containsKey(uuid)) if (this.permissions.containsKey(uuid)) this.removePlayer(uuid); else; else try {
			DatabaseLink link = new DatabaseLink(this.database, "players", uuid.toString());
			link.getDatabaseData();
			this.permissions.put(uuid, null);
			this.players.put(uuid, link);
			return true;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return false;
	}
	
	public final DatabaseLink getPlayer(UUID uuid) {
		return this.players.get(uuid);
	}
	
	public final DatabaseLink getPlayer(Player player) {
		return this.getPlayer(player.getUniqueId());
	}
	
	public DatabaseLink getOfflinePlayer(UUID uuid) throws Exception {
		if (this.players.containsKey(uuid)) return this.getPlayer(uuid);
		DatabaseLink link = new DatabaseLink(this.database, "players", uuid.toString());
		link.getDatabaseData();
		return link;
	}
	
	public void removePlayer(final UUID uuid) {
		if (this.permissions.containsKey(uuid)) this.permissions.remove(uuid);
		if (this.players.containsKey(uuid)) new Thread() {
			public void run() {
				DatabaseLink link = LyneCraft.this.players.get(uuid);
				try {
					link.saveDatabaseData();
				} catch (Exception exception) {
					System.out.println("Data of " + uuid.toString());
					link.displayData();
					exception.printStackTrace();
				}
				LyneCraft.this.players.remove(uuid);
			}
		}.start();
	}
	
	public void removePlayer(Player player) {
		this.removePlayer(player.getUniqueId());
	}
	
	public final Group getGroup(UUID uuid) {
		return Group.valueOf(this.getPlayer(uuid).getData("group", new DatabaseData(Group.DEFAULT.name())).getAsString().toUpperCase());
	}
	
	public final Group getGroup(Player player) {
		return this.getGroup(player.getUniqueId());
	}
	
	public void setPermissions(Player player, String... permissions) {
		PermissionAttachment attachment = new PermissionAttachment(this, player);
		for (String permission : permissions) attachment.setPermission(permission, true);
		if (this.permissions.containsKey(player.getUniqueId())) try {
			PermissionAttachment old = this.permissions.get(player.getUniqueId());
			player.removeAttachment(old);
			old.remove();
		} catch (Exception exception) {
			// Ignore
		}
		this.permissions.put(player.getUniqueId(), attachment);
	}
	
	public void broadcastMessage(Player sender, Message type, String message) {
		for (Player player : this.getServer().getOnlinePlayers()) if (!this.sendMessage(sender, player, type, message)) return;
	}
	
	public boolean sendMessage(Player sender, Player receiver, Message type, String message) {
		return type.send(this, sender, receiver, message);
	}

	public final Team getTeam(String name) {
		Team team = this.getServer().getScoreboardManager().getMainScoreboard().getTeam(name);
		if (team == null) team = this.getServer().getScoreboardManager().getMainScoreboard().registerNewTeam(name);
		return team;
	}

	public final Scoreboard getScoreboard(Player player) {
		Scoreboard scoreboard = this.getServer().getScoreboardManager().getNewScoreboard();
		Objective objective = scoreboard.registerNewObjective("Lyne-Craft", "dummy", ChatColor.GOLD + "Lyne-Craft", RenderType.INTEGER);

		Team players = scoreboard.registerNewTeam("b");
		players.setPrefix(ChatColor.GREEN + "Joueurs ");
		
		Team display = scoreboard.registerNewTeam("c");
		display.setPrefix(ChatColor.BLUE + "mc.");
		
		objective.getScore(ChatColor.GREEN + "Entités").setScore(this.entities);
		
		objective.getScore(ChatColor.GREEN + "au total").setScore(this.getConfig().getInt("total"));
		players.addEntry(ChatColor.GREEN + "au total");
		
		objective.getScore(ChatColor.GREEN + "en ligne").setScore(this.getServer().getOnlinePlayers().size());
		players.addEntry(ChatColor.GREEN + "en ligne");
		
		objective.getScore(ChatColor.GREEN + "Argent").setScore(this.getPlayer(player).getData("money", new DatabaseData(100)).getAsInteger());
		
		objective.getScore(ChatColor.RESET + " " + ChatColor.RESET).setScore(-1);
		
		objective.getScore(ChatColor.BLUE + "lynecraft.fr").setScore(-2);
		display.addEntry(ChatColor.BLUE + "lynecraft.fr");
		
		return scoreboard;
	}

	public final Protection getProtection() {
		return this.protection;
	}
}