package fr.nicolasdemailly.lynecraft.message;

public class MessageException extends Exception {
	private static final long serialVersionUID = 1L;
	
	private final boolean stop;
	private final String message;
	
	public MessageException(boolean stop, String message) {
		this.stop = stop;
		this.message = message;
	}
	
	public MessageException(boolean stop) {
		this(stop, null);
	}
	
	public final boolean isStoped() {
		return this.stop;
	}
	
	@Override
	public final String getMessage() {
		return this.message;
	}
}