package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerLoginEvent;

import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;

public class Ban extends TimedCommand {
	public Ban(LyneCraft plugin) {
		super(plugin, "ban");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender.hasPermission("moderation.ban")) {
			if (arguments.length > 0) {
				int start = label.equalsIgnoreCase("ban") ? 1 : 2;
				UUID uuid = this.getPlugin().getServer().getPlayerUniqueId(arguments[0]);
				if (!(uuid == null)) try {
					String message = " non respect des r�gles du serveur";
					if (arguments.length > start) {
						message = "";
						for (int index = start; index < arguments.length; index++) message += " " + arguments[index];
					}
					DatabaseLink link = this.getPlugin().getOfflinePlayer(uuid);
					if (start == 1) link.setData("ban.time", "infinite"); else link.setData("ban.time", this.getDate(arguments[1]));
					link.setData("ban.reason", message.substring(1));
					link.saveDatabaseData();
					Player player = this.getPlugin().getServer().getPlayer(uuid);
					if (!(player == null)) {
						PlayerLoginEvent event = new PlayerLoginEvent(player, null, null);
						this.getPlugin().getServer().getPluginManager().callEvent(event);
						player.kickPlayer(event.getKickMessage());
					}
					sender.sendMessage(ChatColor.GREEN + "Le joueur a �t� bannis.");
					return true;
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				sender.sendMessage(ChatColor.RED + "Impossible de bannir ce joueur.");
			}
		} else return this.noPermission(sender);
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		List<String> temporaly = new ArrayList<String>();
		if (sender.hasPermission("moderation.ban")) {
			if (arguments.length == 1) for (Player player : this.getPlugin().getServer().getOnlinePlayers()) temporaly.add(player.getName());
			if (label.equalsIgnoreCase("tempban") && arguments.length == 2) {
				temporaly.add("10m");
				temporaly.add("30m");
				temporaly.add("1h");
				temporaly.add("2h");
				temporaly.add("15d");
				temporaly.add("30d");
				temporaly.add("50d");
				temporaly.add("100d");
			}
			if ((label.equalsIgnoreCase("tempban") && arguments.length == 3) || (arguments.length == 2 && !label.equalsIgnoreCase("tempban"))) {
				temporaly.add("non respect des r�gles du serveur");
				temporaly.add("messages contrevers�s");
				temporaly.add("comportement inappropri�");
			}
		}
		return temporaly;
	}
}