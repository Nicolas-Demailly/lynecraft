package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_13_R2.block.data.CraftBlockData;
import org.bukkit.entity.Player;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public class Test extends Command {
	public Test(LyneCraft plugin) {
		super(plugin, "test");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender instanceof Player) {
			Player player = (Player) sender;
			if (arguments.length > 0) {
				player.getChunk().getBlock(0, 0, 0).setBlockData(new TestData(arguments[0]));
			}
			player.sendMessage(((TestData) player.getChunk().getBlock(0, 0, 0).getBlockData()).test);
		} else return this.noPermission(sender);
		return true;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		return new ArrayList<String>();
	}
	
	public class TestData extends CraftBlockData {
		public String test;
		
		public TestData(String test) {
			super();
			this.test = test;
		}
	}
}