package fr.nicolasdemailly.lynecraft.proxy;

import java.util.logging.Logger;

import com.google.inject.Inject;
import com.velocitypowered.api.event.Subscribe;
import com.velocitypowered.api.event.proxy.ProxyInitializeEvent;
import com.velocitypowered.api.plugin.Plugin;
import com.velocitypowered.api.proxy.ProxyServer;

import fr.nicolasdemailly.lynecraft.listener.KickedFromServer;

@Plugin(id = "lynecraft", name = "LyneCraft", version = "1.14-PR0", description = "Lyne-Craft proxy plugin.", authors = { "Nicolas Demailly" })
public class Proxy {
	private final ProxyServer server;
	
	@Inject
	public Proxy(ProxyServer server, Logger logger) {
		this.server = server;
	}
	
	@Subscribe
    public void onProxyInitialize(ProxyInitializeEvent event) {
		this.server.getEventManager().register(this, new KickedFromServer());
    }
}