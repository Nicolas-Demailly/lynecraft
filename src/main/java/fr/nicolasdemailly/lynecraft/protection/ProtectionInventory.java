package fr.nicolasdemailly.lynecraft.protection;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.Chunk;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;

import fr.lyneteam.nico.database.DatabaseData;
import fr.nicolasdemailly.lynecraft.inventory.OpenableInventory;

public class ProtectionInventory extends OpenableInventory {
	private final Map<Player, Map<Chunk, List<UUID>>> players;
	
	public ProtectionInventory() {
		this.players = new HashMap<Player, Map<Chunk, List<UUID>>>();
	}
	
	@Override
	public void open(final Player player) {
		Inventory inventory = this.plugin.getServer().createInventory(player, 54, "Protections");
		
		ItemStack separator = new ItemStack(Material.LIGHT_BLUE_STAINED_GLASS_PANE);
		ItemMeta separatorMeta = separator.getItemMeta();
		separatorMeta.setDisplayName(ChatColor.RED + " " + ChatColor.BLACK);
		separator.setItemMeta(separatorMeta);
	    
	    ItemStack add = new ItemStack(Material.LIME_CONCRETE);
	    ItemMeta addMeta = add.getItemMeta();
	    addMeta.setDisplayName(ChatColor.GOLD + "Supprimer un joueur");
	    List<String> addLore = new ArrayList<String>();
	    addLore.add(ChatColor.GRAY + "Supprimer un joueur de");
	    addLore.add(ChatColor.GRAY + "toutes les protections.");
	    addMeta.setLore(addLore);
	    add.setItemMeta(addMeta);
	    
	    ItemStack remove = new ItemStack(Material.RED_CONCRETE);
	    ItemMeta removeMeta = remove.getItemMeta();
	    removeMeta.setDisplayName(ChatColor.GOLD + "Ajouter un joueur");
	    List<String> removeLore = new ArrayList<String>();
	    removeLore.add(ChatColor.GRAY + "Ajouter un joueur �");
	    removeLore.add(ChatColor.GRAY + "toutes les protections.");
	    removeMeta.setLore(removeLore);
	    remove.setItemMeta(removeMeta);
	    
	    ItemStack reset = new ItemStack(Material.BARRIER);
	    ItemMeta resetMeta = reset.getItemMeta();
	    resetMeta.setDisplayName(ChatColor.GOLD + "Supprimer toutes les protections");
	    List<String> resetLore = new ArrayList<String>();
	    resetLore.add(ChatColor.RED + "Supprimer toutes vos");
	    resetLore.add(ChatColor.RED + "prot�ctions est une");
	    resetLore.add(ChatColor.RED + "op�ration irr�m�diable.");
	    resetMeta.setLore(resetLore);
	    reset.setItemMeta(resetMeta);
	    
	    for (int index = 36; index < 51; index++) inventory.setItem(index, separator);
	    
	    inventory.setItem(51, add);
	    inventory.setItem(52, remove);
	    inventory.setItem(53, reset);
	    
		player.openInventory(inventory);
		
		new Thread() {
			
			public void run() {
				try {
					ResultSet result = ProtectionInventory.this.plugin.getDatabase().executeQuery("SELECT b.w, b.x, b.z, b.p FROM chunks as a, chunks as b WHERE a.o=1 AND a.p='" + player.getUniqueId().toString() + "' AND b.w=a.w AND b.x=a.x AND b.z=a.z");
					Map<Chunk, List<UUID>> list = new HashMap<Chunk, List<UUID>>();
					while (result.next()) {
						Chunk chunk = ProtectionInventory.this.plugin.getServer().getWorld(UUID.fromString(result.getString("w"))).getChunkAt(result.getInt("x"), result.getInt("z"));
						if (!list.containsKey(chunk)) list.put(chunk, new ArrayList<UUID>());
						list.get(chunk).add(UUID.fromString(result.getString("p")));
					}
					ProtectionInventory.this.players.put(player, list);
					ProtectionInventory.this.setPage(player, 1);
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
			
		}.start();
	}
	
	public void setPage(Player player, int index) {
		List<Entry<Chunk, List<UUID>>> list = new ArrayList<Entry<Chunk, List<UUID>>>(this.players.get(player).entrySet());
		
		Inventory inventory = player.getOpenInventory().getTopInventory();
		
		ItemStack previous = new ItemStack(Material.PLAYER_HEAD);
		SkullMeta previousMeta = (SkullMeta) previous.getItemMeta();		
		GameProfile previousProfile = new GameProfile(UUID.fromString("a68f0b64-8d14-4000-a95f-4b9ba14f8df9"), "MHF_ArrowLeft");
		previousProfile.getProperties().put("textures", new Property("textures", "eyJ0aW1lc3RhbXAiOjE1NDc0MDI5OTU3NTQsInByb2ZpbGVJZCI6ImE2OGYwYjY0OGQxNDQwMDBhOTVmNGI5YmExNGY4ZGY5IiwicHJvZmlsZU5hbWUiOiJNSEZfQXJyb3dMZWZ0IiwidGV4dHVyZXMiOnsiU0tJTiI6eyJ1cmwiOiJodHRwOi8vdGV4dHVyZXMubWluZWNyYWZ0Lm5ldC90ZXh0dXJlL2Y3YWFjYWQxOTNlMjIyNjk3MWVkOTUzMDJkYmE0MzM0MzhiZTQ2NDRmYmFiNWViZjgxODA1NDA2MTY2N2ZiZTIifX19"));
		
		ItemStack next = new ItemStack(Material.PLAYER_HEAD);
		SkullMeta nextMeta = (SkullMeta) next.getItemMeta();		
		GameProfile nextProfile = new GameProfile(UUID.fromString("50c8510b-5ea0-4d60-be9a-7d542d6cd156"), "MHF_ArrowRight");
		nextProfile.getProperties().put("textures", new Property("textures", "eyJ0aW1lc3RhbXAiOjE1NDc0MDMyNzAzNzksInByb2ZpbGVJZCI6IjUwYzg1MTBiNWVhMDRkNjBiZTlhN2Q1NDJkNmNkMTU2IiwicHJvZmlsZU5hbWUiOiJNSEZfQXJyb3dSaWdodCIsInRleHR1cmVzIjp7IlNLSU4iOnsidXJsIjoiaHR0cDovL3RleHR1cmVzLm1pbmVjcmFmdC5uZXQvdGV4dHVyZS9kMzRlZjA2Mzg1MzcyMjJiMjBmNDgwNjk0ZGFkYzBmODVmYmUwNzU5ZDU4MWFhN2ZjZGYyZTQzMTM5Mzc3MTU4In19fQ=="));
		
		try {
	    	Field field = previousMeta.getClass().getDeclaredField("profile");
	    	field.setAccessible(true);
	    	field.set(previousMeta, previousProfile);
	    	field.set(nextMeta, nextProfile);
	    } catch (Exception exception) {
	        exception.printStackTrace();
	    }
		
	    int number = list.size() / 36 + 1;
	    
		if (index > 1) previousMeta.setDisplayName(ChatColor.GOLD + "Page pr�c�dente " + ChatColor.RED + "(" + (index - 1) + ")"); else previousMeta.setDisplayName(ChatColor.RED + " " + ChatColor.BLACK); 
		if (index < number) nextMeta.setDisplayName(ChatColor.GOLD + "Page pr�c�dente " + ChatColor.RED + "(" + (index + 1) + ")"); else nextMeta.setDisplayName(ChatColor.RED + " " + ChatColor.BLACK); 
		
	    previous.setItemMeta(previousMeta);
	    next.setItemMeta(nextMeta);
		
	    int chunks = this.plugin.getPlayer(player).getData("chunks", new DatabaseData(4)).getAsInteger();
	    String rest = String.valueOf(chunks);
	    String max = String.valueOf(list.size() + chunks);
	    if (player.hasPermission("moderation.protection.unlimited")) {
	    	rest = "Illimit�";
	    	max = rest;
	    }
	    
	    ItemStack page = new ItemStack(Material.PAPER, index);
	    ItemMeta pageMeta = page.getItemMeta();
	    pageMeta.setDisplayName(ChatColor.GOLD + "Page " + ChatColor.RED + index + " / " + number);
	    List<String> pageLore = new ArrayList<String>();
	    pageLore.add(ChatColor.GRAY + "Cliquez sur les fl�ches");
	    pageLore.add(ChatColor.GRAY + "pour changer de page.");
	    pageMeta.setLore(pageLore);
	    page.setItemMeta(pageMeta);
	    
	    ItemStack profile = new ItemStack(Material.PLAYER_HEAD);
		SkullMeta profileMeta = (SkullMeta) next.getItemMeta();	
		profileMeta.setOwningPlayer(player);
	    profileMeta.setDisplayName(ChatColor.GOLD + "Informations");
	    List<String> profileLore = new ArrayList<String>();
	    profileLore.add(ChatColor.GRAY + "Protections au total : " + ChatColor.YELLOW + list.size());
	    profileLore.add(ChatColor.GRAY + "Protections restantes : " + ChatColor.YELLOW + rest);
	    profileLore.add(ChatColor.GRAY + "Protections maximal : " + ChatColor.YELLOW + max);
	    profileMeta.setLore(profileLore);
	    profile.setItemMeta(profileMeta);
	    
	    inventory.setItem(45, previous);
	    inventory.setItem(46, page);
	    inventory.setItem(47, next);
	    
	    inventory.setItem(49, profile);
	    
	    for (int slot = 0; slot < 36; slot++) {
	    	int temporaly = (index - 1) * 36 + slot;
	    	if (temporaly < list.size()) {
	    		Entry<Chunk, List<UUID>> entry = list.get(temporaly);
	    		List<UUID> players = entry.getValue();
	    		Chunk chunk = entry.getKey();
	    		Block block = chunk.getBlock(8, 8, 8);
	    		int size = entry.getValue().size();
	    		if (size <= 0) size = 1;
	    		ItemStack item = new ItemStack(Material.GRASS_BLOCK, size);
	    		String world = "Principal";
	    		switch (chunk.getWorld().getName()) {
	    			case "world_nether":
	    				world = "Enfer";
	    				item.setType(Material.NETHERRACK);
	    				break;
	    			case "world_the_end":
	    				world = "Sous-monde";
	    				item.setType(Material.END_STONE);
	    				break;
	    		}
	    		ItemMeta itemMeta = item.getItemMeta();
	    		itemMeta.setDisplayName(ChatColor.GOLD + "Protection #" + (temporaly + 1));
	    		List<String> lore = new ArrayList<String>();
	    		lore.add(ChatColor.GRAY + "Monde : " + ChatColor.YELLOW + world);
	    		lore.add(ChatColor.GRAY + "Coordonn�s : " + ChatColor.YELLOW + block.getX() + " " + block.getZ());
	    		players.remove(player.getUniqueId());
	    		if (!players.isEmpty()) {
	    			lore.add(ChatColor.GRAY + "Joueurs " + ChatColor.YELLOW + "(" + players.size() + ")" + ChatColor.GRAY + " :");
	    			for (UUID uuid : players) lore.add(ChatColor.GRAY + "- " + ChatColor.YELLOW + ProtectionInventory.this.plugin.getServer().getOfflinePlayer(uuid).getName());
	    		}
	    		lore.add(ChatColor.RED + " " + ChatColor.BLACK);
	    		lore.add(ChatColor.GRAY + "Clique " + ChatColor.DARK_GRAY + ChatColor.BOLD + "gauche" + ChatColor.GRAY + " pour " + ChatColor.YELLOW + "ajouter un joueur");
	    		lore.add(ChatColor.GRAY + "Clique " + ChatColor.DARK_GRAY + ChatColor.BOLD + "droit" + ChatColor.GRAY + " pour " + ChatColor.YELLOW + "supprimer un joueur");
	    		lore.add(ChatColor.GRAY + "Clique " + ChatColor.DARK_GRAY + ChatColor.BOLD + "molette" + ChatColor.GRAY + " pour " + ChatColor.YELLOW + " pour supprimer la protection");
	    		itemMeta.setLore(lore);
	    		item.setItemMeta(itemMeta);
	    		inventory.setItem(slot, item);
	    	} else inventory.setItem(slot, null);
	    }
	}
	
	@EventHandler
	public void inventoryInteract(InventoryInteractEvent event) {
		if (event.getInventory().getName().equals("Protections")) event.setCancelled(true);
	}
	
	@EventHandler
	public void inventoryClick(InventoryClickEvent event) {
		if (event.getWhoClicked() instanceof Player && event.getInventory().getName().equals("Protections")) {
			Player player = (Player) event.getWhoClicked();
			if (!event.getCurrentItem().getItemMeta().getDisplayName().equals(ChatColor.RED + " " + ChatColor.BLACK)) {
				if (event.getSlot() == 45 || event.getSlot() == 47) {
					this.setPage(player, Integer.parseInt(event.getCurrentItem().getItemMeta().getDisplayName().split("[\\(\\)]")[1]));
				}
			}
			event.setCancelled(true);
		}
	}
}