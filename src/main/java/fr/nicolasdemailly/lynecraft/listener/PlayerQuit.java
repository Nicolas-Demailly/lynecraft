package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.message.Message;

public class PlayerQuit extends Listener {
	public PlayerQuit(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void playerQuit(PlayerQuitEvent event) {
		event.setQuitMessage(null);
		this.getPlugin().broadcastMessage(event.getPlayer(), Message.LOG, ChatColor.GRAY + event.getPlayer().getCustomName() + " s'est déconnecté.");
		this.getPlugin().removePlayer(event.getPlayer());
	}
}