package fr.nicolasdemailly.lynecraft.protection;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Chunk;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;

public class ProtectionInformation extends ProtectionSource {
	private final Chunk chunk;
	private final List<Player> users;
	private final Map<Block, ProtectionSource> blocks;
	private boolean ready;
	
	public ProtectionInformation(Chunk chunk) {
		super("chunks", chunk.getWorld().getUID().toString(), chunk.getX(), -1, chunk.getZ());
		this.chunk = chunk;
		this.users = new ArrayList<Player>();
		this.blocks = new HashMap<Block, ProtectionSource>();
		this.ready = false;
	}

	public void receive(ResultSet result) throws Exception {
		UUID uuid = UUID.fromString(result.getString("p"));
		ProtectionSource source = this;
		if (!(result.getObject("y") == null)) source = this.getBlock(this.chunk.getWorld().getBlockAt(result.getInt("x"), result.getInt("y"), result.getInt("z")), true);
		if (result.getBoolean("o")) source.owner = uuid;
		if (!source.players.contains(uuid)) source.players.add(uuid);
		this.ready = true;
	}
	
	public final boolean isReady() {
		return this.ready;
	}

	public final List<Player> getUsers() {
		return this.users;
	}

	public void addUser(Player player) {
		this.users.add(player);
	}
	
	public void removeUser(Player player) {
		this.users.remove(player);
	}
	
	protected synchronized ProtectionSource getBlock(Block block, boolean increment) {
		if (!this.blocks.containsKey(block)) if (increment) this.blocks.put(block, new ProtectionSource("blocks", block.getWorld().getUID().toString(), block.getX(), block.getY(), block.getZ())); else return new ProtectionSource("blocks", block.getWorld().getUID().toString(), block.getX(), block.getY(), block.getZ());
		return this.blocks.get(block);
	}
}