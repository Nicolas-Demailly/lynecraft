package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;

public class Unmute extends TimedCommand {
	public Unmute(LyneCraft plugin) {
		super(plugin, "unmute");
	}

	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender.hasPermission("moderation.mute")) {
			if (arguments.length > 0) {
				UUID uuid = this.getPlugin().getServer().getPlayerUniqueId(arguments[0]);
				if (!(uuid == null)) try {
					DatabaseLink link = this.getPlugin().getOfflinePlayer(uuid);
					link.removeData("mute.time");
					link.saveDatabaseData();
					sender.sendMessage(ChatColor.GREEN + "Le joueur a d�sormais la parole.");
					return true;
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				sender.sendMessage(ChatColor.RED + "Impossible de rendre la parole � ce joueur.");
			}
		} else return this.noPermission(sender);
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		List<String> temporaly = new ArrayList<String>();
		if (sender.hasPermission("moderation.mute")) if (arguments.length == 1) for (Player player : this.getPlugin().getServer().getOnlinePlayers()) temporaly.add(player.getName());
		return temporaly;
	}
}