package fr.nicolasdemailly.lynecraft.protection;

import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;
import fr.nicolasdemailly.lynecraft.listener.Listener;

public class Protection extends Listener {
	private final Map<Chunk, ProtectionInformation> informations;
	
	public Protection(LyneCraft plugin) {
		super(plugin);
		this.informations = new HashMap<Chunk, ProtectionInformation>();
	}
	
	public synchronized ProtectionInformation getChunk(Chunk chunk, boolean increment) {
		if (!this.informations.containsKey(chunk)) if (increment) this.informations.put(chunk, new ProtectionInformation(chunk)); else try {
			ProtectionInformation information = new ProtectionInformation(chunk);
			Field field = information.getClass().getDeclaredField("ready");
			field.setAccessible(true);
			field.setBoolean(information, true);
			return information;
		} catch (Exception exception) {
			exception.printStackTrace();
		}
		return this.informations.get(chunk);
	}
	
	private void removeChunks(Chunk chunk, Player player) {
		for (int x = -1; x <= 1; x++) for (int z = -1; z <= 1; z++) {
			Chunk temporaly = chunk.getWorld().getChunkAt(chunk.getX() + x, chunk.getZ() + z);
			ProtectionInformation information = this.getChunk(temporaly, false);
			information.removeUser(player);
			if (information.getUsers().isEmpty()) this.informations.remove(temporaly);
		}
	}
	
	private void setupChunks(final Chunk chunk, Player player) {
		for (int x = -1; x <= 1; x++) for (int z = -1; z <= 1; z++) this.getChunk(chunk.getWorld().getChunkAt(chunk.getX() + x, chunk.getZ() + z), true).addUser(player);
		new Thread() {
			public void run() {
				try {
					ResultSet result = Protection.this.getPlugin().getDatabase().executeQuery("(SELECT w, x, NULL AS y, z, o, p FROM chunks WHERE w='" + chunk.getWorld().getUID().toString() + "' AND x<=" + (chunk.getX() + 1) + " AND x>=" + (chunk.getX() - 1) + " AND z<=" + (chunk.getZ() + 1) + " AND z>=" + (chunk.getZ() - 1) + ") UNION "
							+ "SELECT * FROM blocks WHERE w='" + chunk.getWorld().getUID().toString() + "' AND x<" + ((chunk.getX() + 1) * 16 + 16) + " AND x>=" + (chunk.getX() - 1) * 16 + " AND z<" + ((chunk.getZ() + 1) * 16 + 16) + " AND z>=" + (chunk.getZ() - 1) * 16);
					while (result.next()) {
						if (result.getObject("y") == null) {
							Protection.this.informations.get(chunk.getWorld().getChunkAt(result.getInt("x"), result.getInt("z"))).receive(result);
						} else {
							Protection.this.informations.get(chunk.getWorld().getBlockAt(result.getInt("x"), result.getInt("y"), result.getInt("z")).getChunk()).receive(result);
						}
					}
					result.close();
				} catch (Exception exception) {
					exception.printStackTrace();
				}
			}
		}.start();
	}
	
	public boolean isProtected(Player player, Location location) {
		if (ProtectionType.PRIVATE.is(this.getPlugin(), location, player) || ProtectionType.PUBLIC.is(this.getPlugin(), location, player)) return true;
		return false;
	}
	
	@EventHandler
	public void playerMove(PlayerMoveEvent event) {
		Player player = event.getPlayer();
		Chunk old = event.getFrom().getChunk();
		final Chunk replacent = event.getTo().getChunk();
		if (!old.equals(replacent)) {
			this.setupChunks(replacent, player);
			this.removeChunks(old, player);
		}
	}

	@EventHandler
	public void playerJoin(PlayerJoinEvent event) {
		this.setupChunks(event.getPlayer().getChunk(), event.getPlayer());
	}
	
	@EventHandler
	public void playerQuit(PlayerQuitEvent event) {
		this.removeChunks(event.getPlayer().getChunk(), event.getPlayer());
	}
}