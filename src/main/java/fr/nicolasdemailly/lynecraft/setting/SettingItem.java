package fr.nicolasdemailly.lynecraft.setting;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public interface SettingItem {
	public ItemStack get(Player player);
}