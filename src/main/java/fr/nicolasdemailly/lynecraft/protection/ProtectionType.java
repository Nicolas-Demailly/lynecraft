package fr.nicolasdemailly.lynecraft.protection;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public enum ProtectionType {
	PUBLIC(new ProtectionMethods() {

		@Override
		public boolean work(LyneCraft plugin, Player player, Location location) {
			if (!(player == null) && player.hasPermission("build.public")) return false;
			for (String name : plugin.getConfig().getConfigurationSection("public").getKeys(false)) {
				if (location.getWorld().getUID().toString().equals(plugin.getConfig().getString("public." + name + ".world"))
						&& location.getX() >= plugin.getConfig().getInt("public." + name + ".first.x")
						&& location.getX() <= plugin.getConfig().getInt("public." + name + ".second.x")
						&& location.getZ() >= plugin.getConfig().getInt("public." + name + ".first.z")
						&& location.getZ() <= plugin.getConfig().getInt("public." + name + ".second.z")) {
					return true;
				}
			}
			return false;
		}
		
	}),
	PRIVATE(new ProtectionMethods() {

		@Override
		public boolean work(LyneCraft plugin, Player player, Location location) {
			if (player.hasPermission("build.private")) return false;
			ProtectionInformation information = plugin.getProtection().getChunk(location.getChunk(), false);
			ProtectionSource block = information.getBlock(location.getBlock(), false);
			return (information.isReady() && (block.isProtected(player.getUniqueId()) || information.isProtected(player.getUniqueId())));
		}
		
	});
	
	private final ProtectionMethods methods;
	
	private ProtectionType(ProtectionMethods methods) {
		this.methods = methods;
	}
	
	public boolean is(LyneCraft plugin, Location location, Player player) {
		return this.methods.work(plugin, player, location);
	}
}