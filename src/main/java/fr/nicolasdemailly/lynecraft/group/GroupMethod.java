package fr.nicolasdemailly.lynecraft.group;

import org.bukkit.entity.Player;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public abstract class GroupMethod {
	public LyneCraft plugin;
	
	protected abstract void setup(Player player);
	protected abstract String chat(Player player, String message);
}