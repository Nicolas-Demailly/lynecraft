package fr.nicolasdemailly.lynecraft.command;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import fr.lyneteam.nico.database.DatabaseLink;
import fr.nicolasdemailly.lynecraft.LyneCraft;

public class Mute extends TimedCommand {
	public Mute(LyneCraft plugin) {
		super(plugin, "mute");
	}
	
	@Override
	public boolean onCommand(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		if (sender.hasPermission("moderation.mute")) {
			if (arguments.length > 0) {
				int start = label.equalsIgnoreCase("mute") ? 1 : 2;
				UUID uuid = this.getPlugin().getServer().getPlayerUniqueId(arguments[0]);
				if (!(uuid == null)) try {
					String message = " non respect des r�gles du serveur";
					if (arguments.length > start) {
						message = "";
						for (int index = start; index < arguments.length; index++) message += " " + arguments[index];
					}
					DatabaseLink link = this.getPlugin().getOfflinePlayer(uuid);
					if (start == 1) link.setData("mute.time", "infinite"); else link.setData("mute.time", this.getDate(arguments[1]));
					link.setData("mute.reason", message.substring(1));
					link.saveDatabaseData();
					Player player = this.getPlugin().getServer().getPlayer(uuid);
					if (!(player == null)) {
						AsyncPlayerChatEvent event = new AsyncPlayerChatEvent(true, player, "dummy", null);
						this.getPlugin().getServer().getPluginManager().callEvent(event);
					}
					sender.sendMessage(ChatColor.GREEN + "Le joueur ne peut plus parler.");
					return true;
				} catch (Exception exception) {
					exception.printStackTrace();
				}
				sender.sendMessage(ChatColor.RED + "Impossible de couper la parole � ce joueur.");
			}
		} else return this.noPermission(sender);
		return false;
	}

	@Override
	public List<String> onTabComplete(CommandSender sender, org.bukkit.command.Command command, String label, String[] arguments) {
		List<String> temporaly = new ArrayList<String>();
		if (sender.hasPermission("moderation.mute")) {
			if (arguments.length == 1) for (Player player : this.getPlugin().getServer().getOnlinePlayers()) temporaly.add(player.getName());
			if (label.equalsIgnoreCase("tempmute") && arguments.length == 2) {
				temporaly.add("10m");
				temporaly.add("30m");
				temporaly.add("1h");
				temporaly.add("2h");
				temporaly.add("15d");
				temporaly.add("30d");
				temporaly.add("50d");
				temporaly.add("100d");
			}
			if ((label.equalsIgnoreCase("tempmute") && arguments.length == 3) || (arguments.length == 2 && !label.equalsIgnoreCase("tempmute"))) {
				temporaly.add("non respect des r�gles du serveur");
				temporaly.add("messages contrevers�s");
				temporaly.add("comportement inappropri�");
			}
		}
		return temporaly;
	}
}