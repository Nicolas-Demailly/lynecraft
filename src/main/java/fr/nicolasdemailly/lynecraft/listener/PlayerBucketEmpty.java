package fr.nicolasdemailly.lynecraft.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerBucketEmptyEvent;

import fr.nicolasdemailly.lynecraft.LyneCraft;

public class PlayerBucketEmpty extends Listener {
	public PlayerBucketEmpty(LyneCraft plugin) {
		super(plugin);
	}
	
	@EventHandler
	public void playerBucketEmpty(PlayerBucketEmptyEvent event) {
		if (this.getPlugin().getProtection().isProtected(event.getPlayer(), event.getBlockClicked().getLocation())) event.setCancelled(true);
	}
}